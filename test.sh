# install cli for amplify
sudo npm install -g @aws-amplify/cli

aws_user_name="testuser"
aws_key_id="aws_key_id"
aws_secret_key="aws_secret_access_key"
region = "us-east-1"

#configure amplify
# amplify configure

amplify configure set default.region $region
amplify configure set user_name $aws_user_name
amplify configure set aws_access_key_id $aws_key_id
amplify configure set aws_secret_access_key $aws_secret_key

initialize the amplify in app
amplify init